package br.com.severnini.notafiscal;

public class EnviadorDeEmail implements AcaoAposCriarNotaFiscal {

	@Override
	public void executar(NotaFiscal nf) {
		System.out.println("Enviou por e-mail");
	}

}
