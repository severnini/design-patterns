package br.com.severnini.notafiscal;

public interface AcaoAposCriarNotaFiscal {

	void executar(NotaFiscal nf);

}
