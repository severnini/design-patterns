package br.com.severnini.notafiscal;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class NotaFiscalBuilder {

	private String razaoSocial;
	private String cnpj;
	private List<ItemDaNota> itens;
	private double valorTotal;
	private double impostos;
	private Calendar data;
	private String observacoes;
	private List<AcaoAposCriarNotaFiscal> acoesAposCriar;

	public NotaFiscalBuilder(List<AcaoAposCriarNotaFiscal> acoes) {
		data = Calendar.getInstance();
		itens = new ArrayList<ItemDaNota>();
		//acoesAposCriar = new ArrayList<AcaoAposCriarNotaFiscal>();
		acoesAposCriar = acoes;
	}

	//	public NotaFiscalBuilder adicionaAcao(AcaoAposCriarNotaFiscal acao) {
	//		acoesAposCriar.add(acao);
	//		return this;
	//	}

	public NotaFiscalBuilder comRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
		return this;
	}

	public NotaFiscalBuilder comCNPJ(String cnpj) {
		this.cnpj = cnpj;
		return this;
	}

	public NotaFiscalBuilder com(ItemDaNota item) {
		itens.add(item);
		valorTotal += item.getValor();
		impostos += item.getValor() * 0.05;
		return this;
	}

	public NotaFiscalBuilder naDataAtual(Calendar data) {
		if (data != null) {
			this.data = data;
		}
		return this;
	}

	public NotaFiscalBuilder comObservacoes(String observacoes) {
		this.observacoes = observacoes;
		return this;
	}

	public NotaFiscal build() {
		NotaFiscal nf = new NotaFiscal(razaoSocial, cnpj, valorTotal, impostos, data, observacoes, itens);

		for(AcaoAposCriarNotaFiscal acao : acoesAposCriar) {
			acao.executar(nf);
		}

		return nf;
	}

}
