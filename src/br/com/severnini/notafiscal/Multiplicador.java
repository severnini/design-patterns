package br.com.severnini.notafiscal;

public class Multiplicador implements AcaoAposCriarNotaFiscal {

	private double fator;

	public Multiplicador(double fator) {
		this.fator = fator;
	}

	@Override
	public void executar(NotaFiscal nf) {
		System.out.println("Valor da nota com fator de multiplicacao: " + (nf.getValor() * fator));
	}

}
