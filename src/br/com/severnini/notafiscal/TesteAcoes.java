package br.com.severnini.notafiscal;

import java.util.Arrays;

import javax.swing.JButton;

public class TesteAcoes {

	public static void main(String[] args) {
		NotaFiscalBuilder builder = new NotaFiscalBuilder(Arrays.asList(new NotaFiscalDao(), new EnviadorDeEmail(), new Multiplicador(2)));

		//		builder.adicionaAcao(new NotaFiscalDao())
		//		.adicionaAcao(new EnviadorDeEmail())
		//		.adicionaAcao(new Multiplicador(2));


		NotaFiscal notaFiscal1 = builder.comRazaoSocial("Nome da Empresa")
				.comCNPJ("00.123.456/0001-00")
				//.naDataAtual()
				.comObservacoes("Observacoes")
				.com(new ItemDaNota("Item 1", 2.0))
				.com(new ItemDaNota("Item 2", 100.0))
				.com(new ItemDaNota("Item 3", 200.0))
				.com(new ItemDaNota("Item 4", 300.0))
				.build();

		System.out.println(notaFiscal1);

		JButton jb = new JButton();

		//jb.addActionListener(new );

	}

}
