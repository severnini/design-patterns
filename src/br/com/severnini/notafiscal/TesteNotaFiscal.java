package br.com.severnini.notafiscal;

import java.util.ArrayList;
import java.util.Calendar;

public class TesteNotaFiscal {

	public static void main(String[] args) {
		NotaFiscalBuilder builder = new NotaFiscalBuilder(new ArrayList<AcaoAposCriarNotaFiscal>());

		NotaFiscal notaFiscal1 = builder.comRazaoSocial("Nome da Empresa")
				.comCNPJ("00.123.456/0001-00")
				//.naDataAtual()
				.comObservacoes("Observacoes")
				.com(new ItemDaNota("Item 1", 2.0))
				.com(new ItemDaNota("Item 2", 100.0))
				.com(new ItemDaNota("Item 3", 200.0))
				.com(new ItemDaNota("Item 4", 300.0))
				.build();

		System.out.println("Nota Fiscal 1: " + notaFiscal1);


		NotaFiscal notaFiscal2 = builder.comRazaoSocial("Nome da Empresa")
				.comCNPJ("00.123.456/0001-00")
				.naDataAtual(Calendar.getInstance())
				.comObservacoes("Observacoes")
				.com(new ItemDaNotaBuilder().comDescricao("Item 1").comValor(40.0).build())
				.com(new ItemDaNotaBuilder().comDescricao("Item 2").comValor(140.0).build())
				.com(new ItemDaNotaBuilder().comDescricao("Item 3").comValor(50.0).build())
				.com(new ItemDaNotaBuilder().comDescricao("Item 4").comValor(900.0).build())
				.build();

		System.out.println("Nota Fiscal 2: " + notaFiscal2);

	}

}
