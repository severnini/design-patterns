package br.com.severnini.notafiscal;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class NotaFiscal {

	//A classe NotaFiscal tem razão social (String), CNPJ (String), valor bruto (double), impostos (double), data de emissao (Calendar) e observações (String).
	private String razaoSocial;
	private String cnpj;
	private double valor;
	private double impostos;
	private Calendar dataDeEmissao;
	private String observacoes;
	private List<ItemDaNota> itens;


	public NotaFiscal(String razaoSocial, String cnpj, double valor,
			double impostos, Calendar dataDeEmissao, String observacoes,
			List<ItemDaNota> itens) {
		super();
		this.razaoSocial = razaoSocial;
		this.cnpj = cnpj;
		this.valor = valor;
		this.impostos = impostos;
		this.dataDeEmissao = dataDeEmissao;
		this.observacoes = observacoes;
		this.itens = itens;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}


	public String getCnpj() {
		return cnpj;
	}


	public double getValor() {
		return valor;
	}


	public double getImpostos() {
		return impostos;
	}


	public Calendar getDataDeEmissao() {
		return dataDeEmissao;
	}


	public String getObservacoes() {
		return observacoes;
	}


	public List<ItemDaNota> getItens() {
		return itens;
	}

	@Override
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return String
				.format("NotaFiscal [razaoSocial=%s, cnpj=%s, valor=%s, impostos=%s, dataDeEmissao=%s, observacoes=%s, itens=%s]",
						razaoSocial, cnpj, valor, impostos, sdf.format(dataDeEmissao.getTime()),
						observacoes, itens);
	}

}
