package br.com.severnini.imposto;

import br.com.severnini.desconto.Item;
import br.com.severnini.desconto.Orcamento;

public class IHIT extends TemplateDeImpostoCondicional {

	//Crie o imposto IHIT, que tem a seguinte regra: caso existam 2 ítens com o mesmo nome, o imposto deve ser de 13% mais R$100,00.
	//Caso contrário, o valor do imposto deverá ser (1% * o número de ítens no orçamento).

	public IHIT() {
	}

	public IHIT(Imposto outroImposto) {
		super(outroImposto);
	}

	@Override
	protected double minimaTaxacao(Orcamento orcamento) {
		return orcamento.getItens().size() * 0.01 + calculoDoOutroImposto(orcamento);
	}

	@Override
	protected double maximaTaxacao(Orcamento orcamento) {
		return (orcamento.getValor() * 0.13) + 100 + calculoDoOutroImposto(orcamento);
	}

	@Override
	protected boolean deveUsarMaximaTaxacao(Orcamento orcamento) {
		Item itemComp = null;
		for (Item item : orcamento.getItens()) {
			if (itemComp == null) {
				itemComp = item;
			} else {
				if (item.getNome().equals(itemComp.getNome())) {
					return true;
				} else {
					itemComp = item;
				}
			}
		}

		return false;
	}

}
