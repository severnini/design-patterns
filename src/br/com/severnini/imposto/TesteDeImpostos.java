package br.com.severnini.imposto;

import br.com.severnini.desconto.Orcamento;

public class TesteDeImpostos {

	public static void main(String[] args) {
		Imposto iss = new ISS();
		Imposto icms = new ICMS();
		Imposto iccc = new ICCC();

		Orcamento orcamento = new Orcamento(500.0);

		CalculadoraDeImpostos calculadorDeImpostos = new CalculadoraDeImpostos();

		calculadorDeImpostos.realizaCalculo(orcamento, iss);
		calculadorDeImpostos.realizaCalculo(orcamento, icms);

		System.out.println("ICCC");
		calculadorDeImpostos.realizaCalculo(orcamento, iccc);
		calculadorDeImpostos.realizaCalculo(new Orcamento(999), iccc);
		calculadorDeImpostos.realizaCalculo(new Orcamento(1000), iccc);
		calculadorDeImpostos.realizaCalculo(new Orcamento(2999), iccc);
		calculadorDeImpostos.realizaCalculo(new Orcamento(3001), iccc);
		calculadorDeImpostos.realizaCalculo(new Orcamento(5000), iccc);
		calculadorDeImpostos.realizaCalculo(new Orcamento(-10), iccc);



	}

}
