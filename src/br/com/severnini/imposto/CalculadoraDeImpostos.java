package br.com.severnini.imposto;

import br.com.severnini.desconto.Orcamento;

public class CalculadoraDeImpostos {

	public void realizaCalculo(Orcamento orcamento, Imposto imposto) {
		double icms = imposto.calcula(orcamento);
		System.out.println(icms);
	}

}
