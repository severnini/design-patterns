package br.com.severnini.imposto;

import br.com.severnini.desconto.Orcamento;

public class TesteImpostoMuitoAlto {

	public static void main(String[] args) {
		Imposto imposto = new ImpostoMuitoAlto(new ICPP());

		Orcamento orcamento = new Orcamento(600);

		System.out.println(imposto.calcula(orcamento));
	}

}
