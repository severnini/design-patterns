package br.com.severnini.imposto;

import br.com.severnini.desconto.Orcamento;

public class TesteDeImpostosComplexos {

	public static void main(String[] args) {
		Imposto imposto = new ICMS(new ISS(new IHIT()));

		Orcamento orcamento = new Orcamento(800);

		System.out.println(imposto.calcula(orcamento));

	}

}
