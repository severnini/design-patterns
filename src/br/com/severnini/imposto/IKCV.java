package br.com.severnini.imposto;

import br.com.severnini.desconto.Item;
import br.com.severnini.desconto.Orcamento;

public class IKCV extends TemplateDeImpostoCondicional {

	//Já o imposto IKCV, caso o valor do orçamento seja maior que 500,00 e algum
	//item tiver valor superior a 100,00, o imposto a ser cobrado é de 10%; caso contrário 6%.

	public IKCV() {
	}

	public IKCV(Imposto outroImposto) {
		super(outroImposto);
	}

	@Override
	public double minimaTaxacao(Orcamento orcamento) {
		return orcamento.getValor() * 0.06 + calculoDoOutroImposto(orcamento);
	}

	@Override
	public double maximaTaxacao(Orcamento orcamento) {
		return orcamento.getValor() * 0.1 + calculoDoOutroImposto(orcamento);
	}

	@Override
	public boolean deveUsarMaximaTaxacao(Orcamento orcamento) {
		if (orcamento.getValor() > 500 && temItemComValorSuperior(orcamento)) {
			return true;
		}
		return false;
	}

	private boolean temItemComValorSuperior(Orcamento orcamento) {
		for (Item item : orcamento.getItens()) {
			if (item.getValor() > 100) {
				return true;
			}
		}
		return false;
	}

}
