package br.com.severnini.desconto;

public class DescontoMaisCincoItens extends DescontoBase {

	//	private Desconto proximo;
	//
	//	@Override
	//	public double desconta(Orcamento orcamento) {
	//		if (orcamento.getItens().size() > 5) {
	//			return orcamento.getValor() * 0.10;
	//		} else {
	//			return proximo.desconta(orcamento);
	//		}
	//	}
	//
	//	@Override
	//	public void setProximo(Desconto proximo) {
	//		this.proximo = proximo;
	//	}

	@Override
	protected boolean canHandle(Orcamento orcamento) {
		return orcamento.getItens().size() > 5;
	}

	@Override
	protected double calculaDesconto(Orcamento orcamento) {
		return orcamento.getValor() * 0.10;
	}

}
