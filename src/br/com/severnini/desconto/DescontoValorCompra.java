package br.com.severnini.desconto;

public class DescontoValorCompra extends DescontoBase {

	//private Desconto proximo;
	private double valorCompra;


	public DescontoValorCompra(double valorCompra) {
		super();
		this.valorCompra = valorCompra;
	}


	@Override
	protected boolean canHandle(Orcamento orcamento) {
		return orcamento.getValor() > valorCompra;
	}


	@Override
	protected double calculaDesconto(Orcamento orcamento) {
		// TODO Auto-generated method stub
		return orcamento.getValor() * 0.07;
	}

	//	@Override
	//	public double desconta(Orcamento orcamento) {
	//		if (orcamento.getValor() > valorCompra) {
	//			return orcamento.getValor() * 0.07;
	//		} else {
	//			return proximo.desconta(orcamento);
	//		}
	//	}
	//
	//	@Override
	//	public void setProximo(Desconto proximo) {
	//		this.proximo = proximo;
	//	}

}
