package br.com.severnini.desconto;

public class TesteDeDescontos {

	public static void main(String[] args) {


		CalculadorDeDescontos calculador = new CalculadorDeDescontos();

		System.out.println( calculador.calcula(new Orcamento(500)) );
		System.out.println( calculador.calcula(new Orcamento(1000)) );

		Orcamento orcamento = new Orcamento(600);
		orcamento.adicionaItem(new Item("ITEM 1",100.0));
		orcamento.adicionaItem(new Item("ITEM 2",100.0));
		orcamento.adicionaItem(new Item("ITEM 3",100.0));
		orcamento.adicionaItem(new Item("ITEM 4",100.0));
		orcamento.adicionaItem(new Item("ITEM 5",100.0));
		orcamento.adicionaItem(new Item("ITEM 6",100.0));
		System.out.println( calculador.calcula(orcamento) );

		Orcamento lapisCaneta = new Orcamento(300);
		lapisCaneta.adicionaItem(new Item("CANETA",150.0));
		lapisCaneta.adicionaItem(new Item("LAPIS",150.0));
		System.out.println( calculador.calcula(lapisCaneta) );

	}

}
