package br.com.severnini.desconto;

public abstract class DescontoBase implements Desconto {

	private Desconto proximo;

	protected abstract boolean canHandle(Orcamento orcamento);
	protected abstract double calculaDesconto(Orcamento orcamento);

	@Override
	public final double desconta(Orcamento orcamento) {
		if (canHandle(orcamento)) {
			return calculaDesconto(orcamento);
		}
		if (proximo != null) {
			return proximo.desconta(orcamento);
		}
		return 0;
	}

	@Override
	public void setProximo(Desconto proximo) {
		this.proximo = proximo;
	}

}
