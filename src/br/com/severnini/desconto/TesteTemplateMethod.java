package br.com.severnini.desconto;

import br.com.severnini.imposto.ICPP;
import br.com.severnini.imposto.IHIT;
import br.com.severnini.imposto.IKCV;
import br.com.severnini.imposto.Imposto;

public class TesteTemplateMethod {

	public static void main(String[] args) {

		Imposto ikcv = new IKCV();
		Imposto icpp = new ICPP();
		Imposto ihit = new IHIT();

		Orcamento orcamento600 = new Orcamento(600);
		orcamento600.adicionaItem(new Item("ITEM 1", 100));
		orcamento600.adicionaItem(new Item("ITEM 2", 200));
		orcamento600.adicionaItem(new Item("ITEM 3", 300));

		Orcamento orcamento200 = new Orcamento(200);
		orcamento200.adicionaItem(new Item("ITEM 1", 50));
		orcamento200.adicionaItem(new Item("ITEM 2", 150));
		orcamento200.adicionaItem(new Item("ITEM 3", 25));
		orcamento200.adicionaItem(new Item("ITEM 3", 25));

		System.out.println("IKCV");
		System.out.println(ikcv.calcula(orcamento600));
		System.out.println(ikcv.calcula(orcamento200));

		System.out.println("ICPP");
		System.out.println(icpp.calcula(orcamento600));
		System.out.println(icpp.calcula(orcamento200));

		System.out.println("IHIT");
		System.out.println(ihit.calcula(orcamento600));
		System.out.println(ihit.calcula(orcamento200));


	}

}
