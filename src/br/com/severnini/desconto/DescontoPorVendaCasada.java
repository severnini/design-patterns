package br.com.severnini.desconto;

public class DescontoPorVendaCasada extends DescontoBase {

	//	private Desconto proximo;
	//
	//	@Override
	//	public double desconta(Orcamento orcamento) {
	//		if (existe("LAPIS", orcamento) && existe("CANETA", orcamento)) {
	//			return orcamento.getValor() * 0.05;
	//		}
	//		return proximo.desconta(orcamento);
	//	}
	//
	//	@Override
	//	public void setProximo(Desconto proximo) {
	//		this.proximo = proximo;
	//	}

	private boolean existe(String nomeDoItem, Orcamento orcamento) {
		for (Item item : orcamento.getItens()) {
			if (item.getNome().equals(nomeDoItem)) {
				return true;
			}
		}
		return false;
	}

	@Override
	protected boolean canHandle(Orcamento orcamento) {
		return existe("LAPIS", orcamento) && existe("CANETA", orcamento);
	}

	@Override
	protected double calculaDesconto(Orcamento orcamento) {
		return orcamento.getValor() * 0.05;
	}

}
