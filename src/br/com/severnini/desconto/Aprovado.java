package br.com.severnini.desconto;

public class Aprovado implements EstadoDeOrcamento {

	private boolean naoAplicado = true;

	@Override
	public void aplicaDescontoExtra(Orcamento orcamento) {
		if (naoAplicado) {
			orcamento.valor -= orcamento.valor * 0.02;
			naoAplicado = false;
		} else {
			throw new RuntimeException("Desconto extra já foi aplicado!");
		}
	}

	@Override
	public void aprova(Orcamento orcamento) {
		throw new RuntimeException("Orçamento já está aprovado!");
	}

	@Override
	public void reprova(Orcamento orcamento) {
		throw new RuntimeException("Orçamento não pode ser reprovado, pois já está aprovado!");
	}

	@Override
	public void finaliza(Orcamento orcamento) {
		orcamento.estadoAtual = new Finalizado();
	}

}
