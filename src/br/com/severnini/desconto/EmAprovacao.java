package br.com.severnini.desconto;


public class EmAprovacao implements EstadoDeOrcamento {

	private boolean naoAplicado = true;

	@Override
	public void aplicaDescontoExtra(Orcamento orcamento) {
		if (naoAplicado) {
			orcamento.valor -= orcamento.valor * 0.05;
			naoAplicado = false;
		} else {
			throw new RuntimeException("Desconto extra já foi aplicado!");
		}
	}

	@Override
	public void aprova(Orcamento orcamento) {
		orcamento.estadoAtual = new Aprovado();
	}

	@Override
	public void reprova(Orcamento orcamento) {
		orcamento.estadoAtual = new Reprovado();
	}

	@Override
	public void finaliza(Orcamento orcamento) {
		throw new RuntimeException("Orçamentos em aprovação não pode ir diretamente para o estado finalizado!.");
	}

}
