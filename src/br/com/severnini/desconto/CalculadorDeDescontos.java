package br.com.severnini.desconto;

public class CalculadorDeDescontos {

	public double calcula(Orcamento orcamento) {
		Desconto d1 = new DescontoMaisCincoItens();
		Desconto d2 = new DescontoValorCompra(500.0);
		Desconto d3 = new DescontoPorVendaCasada();
		Desconto sem = new SemDesconto();


		d1.setProximo(d2);
		d2.setProximo(d3);
		d3.setProximo(sem);

		return d1.desconta(orcamento);
	}
}
