package br.com.severnini.banco;

import br.com.severnini.banco.conta.Conta;

public class RespostaCSV implements Resposta {

	private Resposta resposta;

	@Override
	public void responde(Requisicao requisicao, Conta conta) {
		if (Formato.CSV.equals(requisicao.getFormato())) {
			System.out.println(conta.getTitular() + "," + conta.getSaldo());
		} else if (resposta!=null) {
			resposta.responde(requisicao, conta);
		}
	}

	public RespostaCSV(Resposta resposta) {
		this.resposta = resposta;
	}

}
