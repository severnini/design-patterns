package br.com.severnini.banco;

import br.com.severnini.banco.conta.Conta;

public class RespostaPorCento implements Resposta {

	private Resposta resposta;

	@Override
	public void responde(Requisicao requisicao, Conta conta) {
		if (Formato.PORCENTO.equals(requisicao.getFormato())) {
			System.out.println(conta.getTitular() + "%" + conta.getSaldo());
		} else if (resposta!=null) {
			resposta.responde(requisicao, conta);
		}
	}

	public RespostaPorCento(Resposta resposta) {
		this.resposta = resposta;
	}
}
