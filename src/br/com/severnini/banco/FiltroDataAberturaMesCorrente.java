package br.com.severnini.banco;

import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.severnini.banco.conta.Conta;

public class FiltroDataAberturaMesCorrente extends Filtro {

	private static SimpleDateFormat SDF = new SimpleDateFormat("MM");

	@Override
	protected boolean checkConta(Conta conta) {
		if (conta.getDataAbertura() != null) {
			String mes = SDF.format(conta.getDataAbertura());
			String mesArual = SDF.format(new Date());
			if (mes.equals(mesArual)){
				return true;
			}
		}

		return false;
	}


}
