package br.com.severnini.banco;

import br.com.severnini.banco.conta.Conta;


public class FiltroSaldoMenor extends Filtro {

	private double saldoMenor;

	public FiltroSaldoMenor(double saldoMenor, Filtro proximoFiltro) {
		super(proximoFiltro);
		this.saldoMenor = saldoMenor;
	}

	@Override
	protected boolean checkConta(Conta conta) {
		return conta.getSaldo() < saldoMenor;
	}

}
