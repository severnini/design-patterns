package br.com.severnini.banco.relatorio;

import java.util.List;

import br.com.severnini.banco.Banco;
import br.com.severnini.banco.conta.Conta;

public class RelatorioSimples extends Relatorio {

	@Override
	protected void imprimeCabecalho(Banco banco) {
		//relatórios simples possuem cabeçalhos e rodapés de uma linha, apenas com o nome do banco e telefone, respectivamente;
		printLine(COLUNAS);
		System.out.println("Banco: " + banco.getNome());
		printLine(COLUNAS);

	}

	@Override
	protected void imprimeRodape(Banco banco) {
		//relatórios simples possuem cabeçalhos e rodapés de uma linha, apenas com o nome do banco e telefone, respectivamente;
		printLine(COLUNAS);
		System.out.println("Telefone: " + banco.getTelefone());
		printLine(COLUNAS);
	}

	@Override
	protected void imprimeCorpo(List<Conta> listaDeContas) {
		//dada uma lista de contas, um relatório simples apenas imprime titular e saldo da conta
		printLine(COLUNAS);
		System.out.println("Titular\t\tSaldo");
		printLine(COLUNAS);
		for (Conta conta : listaDeContas)
			System.out.println(conta.getTitular() + "\t\t" + conta.getSaldo());
	}

}
