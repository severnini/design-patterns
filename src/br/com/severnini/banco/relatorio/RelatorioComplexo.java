package br.com.severnini.banco.relatorio;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.com.severnini.banco.Banco;
import br.com.severnini.banco.conta.Conta;

public class RelatorioComplexo extends Relatorio {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/Y");

	@Override
	protected void imprimeCabecalho(Banco banco) {
		//relatórios complexos possuem cabeçalhos que informam o nome do banco, endereço, telefone
		printLine(COLUNAS);
		System.out.println("Banco: " + banco.getNome());
		System.out.println("Endereço: " + banco.getEndereco());
		System.out.println("Telefone: " + banco.getTelefone());
		printLine(COLUNAS);
	}

	@Override
	protected void imprimeRodape(Banco banco) {
		//relatórios complexos possuem rodapés que informam e-mail, e a data atual
		printLine(COLUNAS);
		System.out.println("E-mail: " + banco.getEmail());
		System.out.println("Data: " + SDF.format(new Date()));
		printLine(COLUNAS);

	}

	@Override
	protected void imprimeCorpo(List<Conta> listaDeContas) {
		//relatório complexo exibe titular, agência, número da conta, e saldo.
		printLine(COLUNAS);
		System.out.println("Titular\t\tAgência\t\tNúmero da conta\t\tSaldo");
		printLine(COLUNAS);
		for (Conta conta : listaDeContas)
			System.out.println(conta.getTitular() + "\t\t" + conta.getAgencia() + "\t\t" + conta.getNumero() + "\t\t" + conta.getSaldo());
	}

}
