package br.com.severnini.banco.relatorio;

import java.util.ArrayList;
import java.util.List;

import br.com.severnini.banco.Banco;
import br.com.severnini.banco.conta.Conta;

public class TesteRelatorio {

	public static void main(String[] args) {

		Banco banco = new Banco("Banco Java", "8909-0876", "R. dos bancos, 15", "banco@banco.com.br");

		List<Conta> lista = new ArrayList<Conta>();

		lista.add(new Conta("Titular 1", 400, "56-3", "898"));
		lista.add(new Conta("Titular 2", 410, "56-3", "123"));
		lista.add(new Conta("Titular 3", 500, "56-3", "78"));
		lista.add(new Conta("Titular 4", 100, "56-3", "12"));
		lista.add(new Conta("Titular 5", -10, "55-2", "09"));
		lista.add(new Conta("Titular 6", 1000, "55-2", "767"));
		lista.add(new Conta("Titular 7", 561.23, "55-2", "143"));
		lista.add(new Conta("Titular 8", 12.1, "55-2", "56"));
		lista.add(new Conta("Titular 9", 09.45, "126-3", "745"));
		lista.add(new Conta("Titular 10", 897, "126-3", "83"));
		lista.add(new Conta("Titular 11", 765, "126-3", "134"));

		Relatorio relatorioSimples = new RelatorioSimples();

		Relatorio relatorioComplexo = new RelatorioComplexo();


		relatorioSimples.geraRelatorio(banco, lista);

		relatorioComplexo.geraRelatorio(banco, lista);


	}

}
