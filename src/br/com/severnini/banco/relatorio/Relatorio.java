package br.com.severnini.banco.relatorio;

import java.util.List;

import br.com.severnini.banco.Banco;
import br.com.severnini.banco.conta.Conta;

public abstract class Relatorio {

	protected static final int COLUNAS = 80;

	protected abstract void imprimeCabecalho(Banco banco);
	protected abstract void imprimeRodape(Banco banco);
	protected abstract void imprimeCorpo(List<Conta> listaDeContas);

	public void geraRelatorio(Banco banco, List<Conta> listaDeContas) {
		imprimeCabecalho(banco);
		imprimeCorpo(listaDeContas);
		imprimeRodape(banco);
	}

	protected void printLine(int count){

		for (int i = 0; i < count-1; i++)
			System.out.print("-");

		System.out.println("-");
	}

}
