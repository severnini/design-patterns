package br.com.severnini.banco;

import br.com.severnini.banco.conta.Conta;

public interface Resposta {

	void responde(Requisicao requisicao, Conta conta);

}
