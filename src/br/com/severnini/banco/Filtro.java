package br.com.severnini.banco;

import java.util.ArrayList;
import java.util.List;

import br.com.severnini.banco.conta.Conta;

public abstract class Filtro {

	private Filtro proximoFiltro;

	public Filtro() {
		proximoFiltro = null;
	}

	public Filtro (Filtro proximoFiltro) {
		this.proximoFiltro = proximoFiltro;
	}

	protected abstract boolean checkConta(Conta conta);

	public final List<Conta> filtra(List<Conta> contas) {

		List<Conta> resultado = new ArrayList<>();

		for (Conta conta : contas) {

			if (checkConta(conta)) {
				resultado.add(conta);
			}

		}

		if (proximoFiltro != null) {
			return proximoFiltro.filtra(resultado);
		}
		return resultado;
	}

}
