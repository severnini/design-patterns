package br.com.severnini.banco;

import br.com.severnini.banco.conta.Conta;

public class RespostaXML implements Resposta {

	private Resposta resposta;

	@Override
	public void responde(Requisicao requisicao, Conta conta) {
		if (Formato.XML.equals(requisicao.getFormato())) {
			System.out.println("<conta><titular>" + conta.getTitular() + "</titular><saldo>" + conta.getSaldo() + "</saldo></conta>");
		} else if (resposta!=null) {
			resposta.responde(requisicao, conta);
		}
	}

	public RespostaXML(Resposta resposta) {
		this.resposta = resposta;
	}

}
