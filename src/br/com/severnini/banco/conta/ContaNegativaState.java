package br.com.severnini.banco.conta;

public class ContaNegativaState implements EstadoDeConta {

	//Uma conta que está negativo, por exemplo, não aceita saques, e depositam apenas 95% do valor total de um depósito efetuado.

	@Override
	public void sacar(Conta conta, double valor) {
		throw new SaldoInsuficienteException("Conta não possui saldo suficiente!");
	}

	@Override
	public void depositar(Conta conta, double valor) {
		conta.saldo += valor * 0.95;
		mudaEstado(conta);
	}

	private void mudaEstado(Conta conta) {
		if (conta.saldo > 0) {
			conta.estadoAtual = new ContaPositivaState();
		}
	}

	@Override
	public String status() {
		return "NEGATIVO";
	}

}
