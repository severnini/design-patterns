package br.com.severnini.banco.conta;

/**
 *
 * Exceção de saldo insuficiente
 *
 * @author Luiz Fernando Severnini
 *
 */
public class SaldoInsuficienteException extends RuntimeException {

	private static final long serialVersionUID = -7361595854593155338L;

	public SaldoInsuficienteException() {
		super();
	}

	public SaldoInsuficienteException(String message) {
		super(message);
	}
}
