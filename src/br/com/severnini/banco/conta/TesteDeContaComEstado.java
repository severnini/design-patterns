package br.com.severnini.banco.conta;

public class TesteDeContaComEstado {

	public static void main(String[] args) {
		runContaTeste("Luiz", 1000);
		runContaTeste("Antonio", -100);
		runContaTeste("Jose", 654.23);
		runContaTeste("Felipe", 235.78);
	}

	private static void runContaTeste(String titular, double valor) {
		Conta conta = new Conta(titular, valor);
		System.out.println(conta);

		try {
			conta.sacar(200);
		} catch (SaldoInsuficienteException e) {
			System.out.println(conta + ": " + e.getMessage());
		}

		System.out.println(conta);

		try {
			conta.sacar(900);
		} catch (SaldoInsuficienteException e) {
			System.out.println(conta + ": " + e.getMessage());
		}
		System.out.println(conta);

		conta.depositar(50);
		System.out.println(conta);
		conta.depositar(50);
		System.out.println(conta);
		conta.depositar(200);
		System.out.println(conta);

	}

}
