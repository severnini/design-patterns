package br.com.severnini.banco.conta;

public class ContaPositivaState implements EstadoDeConta {

	//Uma conta que está com saldo positivo, aceita saques, e o banco deposita 98% do valor do depósito.

	@Override
	public void sacar(Conta conta, double valor) {
		conta.saldo -= valor;
		mudaEstado(conta);
		//		if (valor > conta.saldo) {
		//			conta.saldo -= valor;
		//			mudaEstado(conta);
		//		} else {
		//			throw new SaldoInsuficienteException("Conta não possui saldo suficiente!");
		//		}
	}

	@Override
	public void depositar(Conta conta, double valor) {
		conta.saldo += valor * 0.98;
	}

	private void mudaEstado(Conta conta) {
		if (conta.saldo <= 0) {
			conta.estadoAtual = new ContaNegativaState();
		}
	}

	@Override
	public String status() {
		return "POSITIVO";
	}

}
