package br.com.severnini.banco.conta;

import java.util.Date;

public class Conta {

	private String titular;
	protected double saldo;
	private String agencia;
	private String numero;
	private Date dataAbertura;
	protected EstadoDeConta estadoAtual;

	public Conta(double saldo) {
		this.saldo = saldo;
	}

	public Conta(String titular, double saldo) {
		super();
		this.titular = titular;
		this.saldo = saldo;

		if (saldo > 0) {
			estadoAtual = new ContaPositivaState();
		} else {
			estadoAtual = new ContaNegativaState();
		}
	}

	public Conta(String titular, double saldo, String agencia,
			String numeroConta) {
		super();
		this.titular = titular;
		this.saldo = saldo;
		this.agencia = agencia;
		numero = numeroConta;
	}

	public String getTitular() {
		return titular;
	}

	public double getSaldo() {
		return saldo;
	}

	public String getAgencia() {
		return agencia;
	}

	public String getNumero() {
		return numero;
	}

	public void depositar(double valor) {
		estadoAtual.depositar(this, valor);
	}

	public void sacar(double valor) {
		estadoAtual.sacar(this, valor);
	}

	public String status() {
		return estadoAtual.status();
	}

	public Date getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	@Override
	public String toString() {
		return String.format("Conta [titular=%s, saldo=%s, agencia=%s, numero=%s, dataAbertura=%s, status()=%s]",
				titular, saldo, agencia, numero, dataAbertura, status());
	}

}
