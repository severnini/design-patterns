package br.com.severnini.banco;

import br.com.severnini.banco.conta.Conta;

public class TesteDeResposta {

	public static void main(String[] args) {
		Resposta repostaPorCento = new RespostaPorCento(null);
		Resposta respostaXML = new RespostaXML(repostaPorCento);
		Resposta respostaCSV = new RespostaCSV(respostaXML);

		Conta conta = new Conta("Luiz", 1000);

		Requisicao requisicaoCSV = new Requisicao(Formato.CSV);
		Requisicao requisicaoXML = new Requisicao(Formato.XML);
		Requisicao requisicaoPorCento = new Requisicao(Formato.PORCENTO);

		respostaCSV.responde(requisicaoCSV, conta);
		respostaCSV.responde(requisicaoXML, conta);
		respostaCSV.responde(requisicaoPorCento, conta);


	}
}
