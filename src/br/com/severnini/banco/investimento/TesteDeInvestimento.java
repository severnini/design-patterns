package br.com.severnini.banco.investimento;

import br.com.severnini.banco.conta.Conta;

public class TesteDeInvestimento {

	public static void main(String[] args) {
		Investimento conservador = new Conservador();
		Investimento moderado = new Moderado();
		Investimento arrojado = new Arrojado();

		RealizadorDeInvestimentos realizadorDeInvestimentos = new RealizadorDeInvestimentos();

		Conta contaBancaria1 = new Conta(1000);
		System.out.println("contaBancaria1: " + contaBancaria1.getSaldo());
		realizadorDeInvestimentos.investir(conservador, contaBancaria1);
		System.out.println("contaBancaria1: " + contaBancaria1.getSaldo());

		Conta contaBancaria2 = new Conta(1000);
		System.out.println("contaBancaria2: " + contaBancaria2.getSaldo());
		realizadorDeInvestimentos.investir(moderado, contaBancaria2);
		System.out.println("contaBancaria2: " + contaBancaria2.getSaldo());

		Conta contaBancaria3 = new Conta(1000);
		System.out.println("contaBancaria3: " + contaBancaria3.getSaldo());
		realizadorDeInvestimentos.investir(arrojado, contaBancaria3);
		System.out.println("contaBancaria3: " + contaBancaria3.getSaldo());

		System.out.println("------------------------------------------------");
	}

}
