package br.com.severnini.banco.investimento;

import br.com.severnini.banco.conta.Conta;

public class Moderado implements Investimento {

	@Override
	public double calculaLucro(Conta conta) {
		boolean escolhido = new java.util.Random().nextDouble() > 0.50;

		if (escolhido) {
			return conta.getSaldo() * 0.007;
		} else {
			return conta.getSaldo() * 0.025;
		}
	}

	//	private Random random;
	//
	//	public Moderado() {
	//		random = new Random();
	//	}
	//
	//	@Override
	//	public double calculaLucro(ContaBancaria conta) {
	//		if (random.nextInt(2) == 0) {
	//			return conta.getSaldo() * 0.025;
	//		} else {
	//			return conta.getSaldo() * 0.007;
	//		}
	//	}


}
