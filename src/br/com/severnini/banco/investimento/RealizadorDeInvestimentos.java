package br.com.severnini.banco.investimento;

import br.com.severnini.banco.conta.Conta;

public class RealizadorDeInvestimentos {

	public void investir(Investimento investimento, Conta contaBancaria) {
		double lucro = investimento.calculaLucro(contaBancaria);

		double liquido = lucro * 0.75;

		contaBancaria.depositar(liquido);
	}

}
