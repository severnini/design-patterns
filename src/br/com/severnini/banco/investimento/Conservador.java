package br.com.severnini.banco.investimento;

import br.com.severnini.banco.conta.Conta;

public class Conservador implements Investimento {

	@Override
	public double calculaLucro(Conta conta) {
		return conta.getSaldo() * 0.008;
	}

}
