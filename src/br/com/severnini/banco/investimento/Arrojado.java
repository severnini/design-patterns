package br.com.severnini.banco.investimento;

import br.com.severnini.banco.conta.Conta;


public class Arrojado implements Investimento {

	@Override
	public double calculaLucro(Conta conta) {
		double chances = new java.util.Random().nextDouble();

		if (chances <= 0.20 ) {
			return conta.getSaldo() * 0.05;
		} else if (chances < 0.50) {
			return conta.getSaldo() * 0.03;
		} else {
			return conta.getSaldo() * 0.006;
		}
	}
	//"ARROJADO", que tem 20% de chances de retornar 5%, 30% de chances de retornar 3%, e 50% de chances de retornar 0.6%
	//	private Random random;
	//
	//	public Arrojado() {
	//		random = new Random();
	//	}
	//
	//	@Override
	//	public double calculaLucro(ContaBancaria contaBancaria) {
	//		int chances = random.nextInt(10);
	//
	//		if(chances >= 0 && chances <= 1) {
	//			return contaBancaria.getSaldo() * 0.05;
	//		} else if (chances >= 2 && chances <= 4) {
	//			return contaBancaria.getSaldo() * 0.03;
	//		}	else {
	//			return contaBancaria.getSaldo() * 0.006;
	//		}
	//	}
}
