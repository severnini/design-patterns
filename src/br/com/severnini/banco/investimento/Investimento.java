package br.com.severnini.banco.investimento;

import br.com.severnini.banco.conta.Conta;


public interface Investimento {

	double calculaLucro(Conta conta);

}
