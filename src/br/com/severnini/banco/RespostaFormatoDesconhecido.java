package br.com.severnini.banco;

import br.com.severnini.banco.conta.Conta;

public class RespostaFormatoDesconhecido implements Resposta {

	@Override
	public void responde(Requisicao requisicao, Conta conta) {
		System.out.println("FORMATO DESCONHECIDO");
	}

	//	@Override
	//	public void setProxima(Resposta resposta) {
	//
	//	}

}
