package br.com.severnini.banco;

import br.com.severnini.banco.conta.Conta;


public class FiltroSaldoMaior extends Filtro {


	private double saldoMaior;

	public FiltroSaldoMaior(double saldo, Filtro proximoFiltro) {
		super(proximoFiltro);
		saldoMaior = saldo;
	}

	@Override
	protected boolean checkConta(Conta conta) {
		return conta.getSaldo() < saldoMaior;
	}

}
